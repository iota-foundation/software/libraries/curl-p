#ifndef __HAMMING_H__
#define __HAMMING_H__

#include "curl_p/pearl_diver.h"
#include "curl_p/trit.h"

PearlDiverStatus hamming(Curl *const, unsigned short const offset,
                         unsigned short const end,
                         unsigned short const security);

#endif
