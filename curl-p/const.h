
#ifndef __CURLP_CONSTANTS_H__
#define __CURLP_CONSTANTS_H__

#define HASH_LENGTH 243
#define STATE_LENGTH 3 * HASH_LENGTH

typedef enum {
	CURL_P_27 = 27,
	CURL_P_81 = 81,
} CurlType;

#endif
