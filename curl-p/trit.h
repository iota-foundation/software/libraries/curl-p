
#ifndef CURL_H
#define CURL_H
#include <trinary/stdint.h>
#include <trinary/trits.h>
#include "curl_p/const.h"

typedef struct {
  trit_t state[STATE_LENGTH];
  CurlType type;
} Curl;

void init_curl(Curl* ctx);

void curl_absorb(Curl* ctx, trit_t* const trits, size_t length);
void curl_squeeze(Curl* ctx, trit_t* const trits, size_t length);
void curl_reset(Curl* ctx);

#endif
