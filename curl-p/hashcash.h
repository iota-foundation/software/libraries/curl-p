#ifndef __HASHCASH_H__
#define __HASHCASH_H__

#include "curl_p/pearl_diver.h"
#include "curl_p/trit.h"

typedef enum {
  TAIL,
  BODY,
  HEAD,
} SearchType;

PearlDiverStatus hamming(Curl *const ctx, unsigned short const offset,
                         unsigned short const end,
                         unsigned short const security);
#endif  // __HASHCASH_H__
