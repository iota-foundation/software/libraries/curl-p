#ifndef __PDTH_SEARCH_H__
#define __PDTH_SEARCH_H__

#include "curl_p/pearl_diver.h"
#include "curl_p/ptrit.h"
#include "curl_p/trit.h"

PearlDiverStatus pd_search(Curl *, unsigned short const, unsigned short const,
                           short (*)(PCurl *, unsigned short), unsigned short);

#endif  //__PDTH_SEARCH_H__
